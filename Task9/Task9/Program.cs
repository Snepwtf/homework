﻿using System;
using System.Collections.Generic;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {

            Timer timer1 = new Timer("Чтение задания", 5);
            Timer timer2 = new Timer("Выполнение задания", 5);
            Timer timer3 = new Timer("Отправка задания", 5);

            List<ICutDownNotifier> timers = new List<ICutDownNotifier> 
            { 
            new MethodHandler(timer1),
            new AnonDelegateHandler(timer2),
            new LambdaHandler(timer3)
            };

            foreach(ICutDownNotifier timer in timers)
            {
                timer.Init();
                timer.Run(5);
            }


        }


    }
}
