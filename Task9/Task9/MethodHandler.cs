﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{
    public class MethodHandler : ICutDownNotifier
    {
        private Timer _timer { get; set; }

        public MethodHandler(Timer timer)
        {
            _timer = timer;

        }

        public void Init()
        {
            _timer.StartTimer += Timer_StartTimer;
            _timer.TickTimer += Timer_TickTimer;
            _timer.FinishTimer += _timer_FinishTimer;
        }

        private void _timer_FinishTimer(object sender, TimerEventArgs eventArgs)
        {

            Console.WriteLine(_timer.Name + "\t" + "Конец");
        }


        private void Timer_TickTimer(object sender, TimerEventArgs eventArgs)
        {
            Console.WriteLine("Тик-Так");

            Console.WriteLine("Осталось : \n " + eventArgs.Seconds);
        }

        private void Timer_StartTimer(object sender, TimerEventArgs eventArgs)
        {

            Console.WriteLine(_timer.Name + "\t" + "Таймер пошёл");
        }

        public void Run(int seconds)
        {
            _timer.Run(seconds);
        }
    }
}
