﻿namespace Task9
{
    public interface ICutDownNotifier
    {
        void Init();
        void Run(int seconds);
    }
}