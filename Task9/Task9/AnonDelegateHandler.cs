﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{

    public class AnonDelegateHandler : ICutDownNotifier
    {
        private Timer _timer { get; set; }
       

        public AnonDelegateHandler(Timer timer)
        {
            _timer = timer;
           
        }

        public void Init()
        {
            _timer.StartTimer += delegate (object sender, TimerEventArgs eventArgs)
            {
                Console.WriteLine(_timer.Name + "\t" + "Таймер пошёл");
            };
            _timer.TickTimer += delegate (object sender, TimerEventArgs eventArgs)
            {
                Console.WriteLine("Тик-Так");
                Console.WriteLine("Осталось : \n " + eventArgs.Seconds);
            };
            _timer.FinishTimer += delegate (object sender, TimerEventArgs eventArgs)
            {
                
                Console.WriteLine(_timer.Name + "\t" + "Конец");
            };

        }

        public void Run(int seconds)
        {
            _timer.Run(seconds);
        }
    }
}
