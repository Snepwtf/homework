﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{
    public class TimerEventArgs
    {
        public int Seconds { get; set; }
        //public string Name { get; set; }
        public TimerEventArgs(int seconds)
        {
            Seconds = seconds;
            //Name = name;
        }
    }
}
