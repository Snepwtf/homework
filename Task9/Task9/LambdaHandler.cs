﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9
{
    public class LambdaHandler : ICutDownNotifier
    {
        private Timer _timer { get; set; }
       

        public LambdaHandler(Timer timer)
        {
            _timer = timer;
           
        }

        public void Init()
        {
            _timer.StartTimer += (object sender, TimerEventArgs eventArgs) => Console.WriteLine(_timer.Name + "\t" + "Таймер пошел");
            _timer.TickTimer += (object sender, TimerEventArgs eventArgs) =>
            {
                Console.WriteLine("Тик-Так");
                Console.WriteLine("Осталось : \n " + eventArgs.Seconds);
            };

            _timer.FinishTimer += (object sender, TimerEventArgs eventArgs) => Console.WriteLine(_timer.Name + "\t" + "Конец");
        }

        public void Run(int seconds)
        {
            _timer.Run(seconds);
        }
    }
}
