﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task9
{
    public delegate void TimerDelegate(object sender, TimerEventArgs eventArgs);

    public class Timer
    {
        public string Name { get; set; }
        public int Seconds { get; set; }
        public Timer(string name, int seconds)
        {
            Name = name;
            Seconds = seconds;
        }

        public event TimerDelegate StartTimer;
        public event TimerDelegate TickTimer;
        public event TimerDelegate FinishTimer;

        public void Run(int seconds)
        {
            StartTimer?.Invoke(this, null);
            for (int i = seconds; i >= 0; i--)
            {
                Thread.Sleep(1000);
                //Console.WriteLine("Осталось : " + i);
                TickTimer?.Invoke(this, new TimerEventArgs(i));
            }
            FinishTimer?.Invoke(this, null);
        }




    }
}
