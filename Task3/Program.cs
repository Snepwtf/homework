﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            double integer = 0;
            double extent = 0;

            
            try
            {
                Console.WriteLine("Введите число корень n-ой степени которого вы хотите узнать");
                integer = double.Parse(Console.ReadLine());

                Console.WriteLine("Введите степень числа");
                extent = double.Parse(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Вводить нужно исключительно числа");
                Console.ReadLine();
                return;
                
            }


            var result = SquareNumber(integer, extent);
            Console.WriteLine($"Корень числа {integer} в степени {extent} равен {result}");

            Console.WriteLine("Хотели бы вы сравнить полученное число с числом,которое вернет метод Math.Pow ?");
            Console.WriteLine("Да/Нет");

            string operation = Console.ReadLine();
            switch (operation)
            {
                case "Да":
                    {
                        var powNumber = integer - Math.Pow(result, extent);
                        //Console.WriteLine(string.Format("{0:0.####################}", powNumber));
                        Console.WriteLine($"Разница чисел {powNumber.ToString("f20")}");
                        break;
                    }
                case "Нет":
                    {
                        Console.WriteLine("Спасибо за использование нашего продукта");
                        break;
                    }
            }
            Console.ReadKey();
        }
        public static double SquareNumber(double A, double n)
        {

            var b = 0.001;
            var x0 = A / n;
            var x1 = (1 / n) * ((n - 1) * x0 + A / Math.Pow(x0, (n - 1)));

            while (Math.Abs(x1 - x0) > b)
            {
                x0 = x1;
                x1 = (1 / n) * ((n - 1) * x0 + A / Math.Pow(x0, (n - 1)));
            }
            return x1;
        }
    }
}
