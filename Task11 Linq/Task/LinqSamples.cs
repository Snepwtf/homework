﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SampleSupport;
using Task;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {
        private readonly DataSource dataSource = new DataSource();

        private bool ValidateZipCode(string val)
        {
            long number;
            return long.TryParse(val, out number);
        }

        private bool ValidatePhone(string phone)
        {
            var pattern = @"^(\([0-9]\))+?";
            return Regex.IsMatch(phone, pattern);
        }

        private List<GroupPriceEntity> SortProductsByPrice()
        {
            var sortedProducts = new List<GroupPriceEntity>();

            foreach (var prod in dataSource.Products)
                if (prod.UnitPrice <= 20M)
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 0 });
                else if ((prod.UnitPrice > 20M) && (prod.UnitPrice <= 50M))
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 1 });
                else
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 2 });

            return sortedProducts;
        }


        [Category("Restriction Operators")]
        [Title("Task 0")]
        [Description("Описание задачи на русском языке")
        ]
        public void LinqForExample()
        {
            var customers = dataSource
                .Customers
                .Select(c => c);

            //Он умеет выполнять запросы самостоятельно. Так же умеет делать foreach по коллекции. 
            //Проверьте это все запустив приложение и сделав запуск запроса
            ObjectDumper.Write(customers);
        }
        [Category("Restriction Operators")]
        [Title("Task 1")]
        [Description("Клиенты чей суммарный оборот больше X")
            ]
        public void Linq001()
        {
            var customers = dataSource
               .Customers
               .Select(c => c.Orders.Sum(sum => sum.Total))
               .Where(sum => sum > 3000);


            ObjectDumper.Write(customers);

        }
        [Category("Restriction Operators")]
        [Title("Task 2")]
        [Description("Клиенты и покупатели в 1 городе и 1 стране")
            ]
        public void Linq002()
        {
            var customers = dataSource.Customers;
            var suppliers = dataSource.Suppliers;

            var result = customers.Join(suppliers,
                c => c.City,
                s => s.City,
                (c, s) => new { Name = c.CompanyName, City = c.City, Country = c.Country, SuplierName = s.SupplierName, SuplierCity = s.City, SuplierCountry = s.Country });


            ObjectDumper.Write(result);

        }
        [Category("Restriction Operators")]
        [Title("Task 3")]
        [Description("Клиенты заказы которых больше X")
            ]
        public void Linq003()
        {
            var customers = dataSource
                .Customers
                .SelectMany(c => c.Orders,
                (c, pr) => new { Name = c.CompanyName, Total = pr.Total })
                .Where(c => c.Total > 600);

            //.SelectMany(c => c.Orders)
            //.Where(c => c.Total > 600);

            ObjectDumper.Write(customers);

        }
        [Category("Restriction Operators")]
        [Title("Task 4")]
        [Description("С какого месяца стали клиентами")
            ]
        public void Linq004()
        {
            var customers = dataSource
                .Customers
                .SelectMany(cust => cust.Orders,
                (c, pr) => new { Name = c.CompanyName, OrderDate = pr.OrderDate })
                .OrderBy(cust => cust.OrderDate);


            //.OrderByDescending(od => od.Orders)
            //.SelectMany(cust => cust.Orders,
            //(cust, od) => new { OrderDate = cust.Orders, Total = od.OrderDate });


            ObjectDumper.Write(customers);
        }

        [Category("Restriction Operators")]
        [Title("Task 5")]
        [Description("Сортировка клиентов по году,месяцу,имени")
            ]
        public void Linq005()
        {
            var customers = dataSource
                  .Customers
                  .SelectMany(cust => cust.Orders,
                  (c, pr) => new { Name = c.CompanyName, OrderDate = pr.OrderDate, Total = pr.Total })
                  .OrderByDescending(cust => cust.Total);


            ObjectDumper.Write(customers);
        }

        [Category("Restriction Operators")]
        [Title("Task 6")]
        [Description("Указан код,не заполнен регион")
           ]
        public void Linq006()
        {

        }
        [Category("Restriction Operators")]
        [Title("Task 7")]
        [Description("Категории,наличие на складе,стоимость")
           ]
        public void Linq007()
        {
            var products = dataSource.Products;

            var result = products.OrderBy(p => p.Category).ThenBy(p => p.UnitsInStock).ThenBy(p => p.UnitPrice);
                //.OrderBy(p => p.Category);
                //.ThenBy(p => p.UnitsInStock)
                //.ThenBy(p => p.UnitPrice);

            ObjectDumper.Write(result);

        }
        [Category("Restriction Operators")]
        [Title("Task 8")]
        [Description("Дешевые,средние,дорогие")
          ]
        public void Linq008()
        {
            var products = dataSource.Products;

            var cheapResult = products.Where(p => p.UnitPrice > 100 || p.UnitPrice < 400).Select(p => new { Name = p.ProductName, UnitPrice = p.UnitPrice });
            ObjectDumper.Write(cheapResult);
            var middleResult = products.Where(p => p.UnitPrice > 450 || p.UnitPrice < 700).Select(p => new { Name = p.ProductName, UnitPrice = p.UnitPrice });
            ObjectDumper.Write(middleResult);
            var expensiveResult = products.Where(p => p.UnitPrice > 800 || p.UnitPrice < 1300).Select(p => new { Name = p.ProductName, UnitPrice = p.UnitPrice });
            ObjectDumper.Write(expensiveResult);
        }
        [Category("Restriction Operators")]
        [Title("Task 9")]
        [Description("Средняя прибыль,средняя интенсивность")
         ]
        public void Linq009()
        {

        }
        [Category("Restriction Operators")]
        [Title("Task 10")]
        [Description("Средне годовая статистика активности по месяцам")
         ]
        public void Linq010()
        {

        }
    }

}