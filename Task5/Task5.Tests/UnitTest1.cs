﻿using System;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task5.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPerimetr()
        {
            //arrange

            double expected = 45;
            Triangle triangle = new Triangle(15, 10, 20);

            //act
            double actual = triangle.Perimetr();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSquare()
        {
            //arrange
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            string expected = "204.53835";
            Triangle triangle = new Triangle(20, 25, 40);
            //act
            var actual = triangle.Square().ToString(("f5"));
            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
