﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    public class Triangle
    {
        private readonly double _aSide, _bSide, _cSide;



        public Triangle(double aSide, double bSide, double cSide)
        {
            _aSide = aSide;
            _bSide = bSide;
            _cSide = cSide;
        }
        /// <summary>
        /// Проверят может ли треугольник с заданными сторонами существовать.
        /// </summary>
        /// <param name="a">сторона А</param>
        /// <param name="b">сторона В</param>
        /// <param name="c">сторона С</param>
        public void TriangleIsReal(double a, double b, double c)
        {
            if ((a + b) < c || (b + c) < a || (c + a) < b)
            {
                //Console.WriteLine("Треугольник с данными сторонами существовать не может");

                throw new Exception("Треугольник с данными сторонами существовать не может");

            }
            else
            {
                Console.WriteLine("Треугольник с данными сторонами может существовать");

            }
        }
        /// <summary>
        /// Вычисление площади треугольника.
        /// Формула Герона
        /// </summary>
        /// <returns></returns>
        public double Square()
        {
            double halfPerimeter = Perimetr() / 2;
            return Math.Sqrt(halfPerimeter * (halfPerimeter - _aSide) * (halfPerimeter - _bSide) * (halfPerimeter - _cSide));
        }
        /// <summary>
        /// Вычисление периметра треугольника.
        /// Формула : a+b+c
        /// </summary>
        /// <returns></returns>
        public double Perimetr()
        {
            return _aSide + _bSide + _cSide;
        }
    }
}
