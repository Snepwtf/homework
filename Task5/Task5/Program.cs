﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Введите стороны треугольника A,B,C");
            Console.WriteLine("Сторона A = ");
            double inputA = double.Parse(Console.ReadLine());
            Console.WriteLine("Сторона B = ");
            double inputB = double.Parse(Console.ReadLine());
            Console.WriteLine("Сторона C = ");
            double inputC = double.Parse(Console.ReadLine());


            Triangle triangle = new Triangle(inputA, inputB, inputC);

            triangle.TriangleIsReal(inputA, inputB, inputC);


            double perimetrResult = triangle.Perimetr();
            Console.WriteLine($"Периметр треугольника равен {perimetrResult}");

            double squareResult = triangle.Square();
            Console.WriteLine($"Площадь треугольника равна {squareResult}");

            Console.ReadLine();

        }

    }
}
