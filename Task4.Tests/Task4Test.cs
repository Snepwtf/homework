﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task4.Tests
{
    [TestClass]
    public class Task4Test
    {
        [TestMethod]
        public void TestEuclideanMethod_1()
        {
            //arrange
            int a = 624960;
            int b = 49104;
            int expected = 4464;
            var ex = new EuclideanClass();

            //act
            int actual = ex.Euclidean(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestEuclideanMethod_2()
        {
            //arrange
            int a = 600;
            int b = 100;
            int c = 200;
            int expected = 100;
            var ex = new EuclideanClass();

            //act
            int actual = ex.Euclidean(a, b, c);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestEuclideanMethod_3()
        {
            //arrange
            int a = 1000;
            int b = 320;
            int c = 670;
            int d = 4500;
            int expected = 10;
            var ex = new EuclideanClass();

            //act
            int actual = ex.Euclidean(a, b, c, d);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestBinaryEuclidean()
        {
            //arrange 
            int a = 20456;
            int b = 5218;
            int expected = 2;
            var ex = new EuclideanClass();

            //act
            int actual = ex.BinaryEuclidean(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

    }
}
