﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10_Collection_Generic
{
    public class Node<T> 
        where T : IComparable<T>
    {
        public T Data { get; set; }
        public Node<T> Left { get; set; }
        public Node<T> Right { get; set; }
        public Node(T data,Node<T> left,Node<T> right)
        {
            Data = data;
            Left = left;
            Right = right;

        }
        public Node(T data)
        {
            Data = data;
        }
        public void Add(T data)
        {
            var node = new Node<T>(data);
            if (node.Data.CompareTo(this.Data) == -1)
            {
                if(Left == null)
                {
                    Left = node;
                }
                else
                {
                    Left.Add(data);
                }
            }
            else
            {
                if(Right == null)
                {
                    Right = node; 
                }
                else
                {
                    Right.Add(data);
                }
            }
        }
    }
}
