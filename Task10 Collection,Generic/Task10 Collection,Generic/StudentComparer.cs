﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10_Collection_Generic
{
    class StudentComparer : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            if (x.Assessment > y.Assessment)
                return 1;
            else if (x.Assessment < y.Assessment)
                return -1;
            else
                return 0;
        }
    }
}
