﻿using System;
using System.Collections.Generic;

namespace Task10_Collection_Generic
{
    class Program
    {
        static void Main(string[] args)
        {
            //var tree = new BinaryTree<int>();
            //tree.Add(4);
            //tree.Add(8);
            //tree.Add(1);
            //tree.Add(3);
            //tree.Add(9);
            //tree.Add(2);
            //tree.Add(5);

            //foreach(int i in tree.GetReversedElements())
            //{
            //    Console.WriteLine(i);
            //}

            List<Student> students = new List<Student> {
                new Student("Oleg",5),
                new Student("Kolya",2),
                new Student("Dasha",1),
                new Student("Natasha",7) };

            BinaryTree<Student> studentsTree = new BinaryTree<Student>(students);

            foreach(Student student in studentsTree.GetReversedElements())
            {
                Console.WriteLine($"{student.Assessment}");
            }

            List<string> strings = new List<string> {
            "Anderson",
            "Louie",
            "Epam"
            };


            BinaryTree<string> stringTree = new BinaryTree<string>(strings);
            
            foreach(string str in stringTree.GetReversedElements())
            {
                Console.WriteLine(str);
            }

            int[] numbers = new int[] { 24, 1, 67, 242, 43, 17 };


            BinaryTree<int> arrayTree = new BinaryTree<int>(numbers);
            arrayTree.Add(13);
            arrayTree.Add(99);

            foreach(int i in arrayTree.GetElements())
            {
                Console.WriteLine(i);
            }
            



        }
    }
}
