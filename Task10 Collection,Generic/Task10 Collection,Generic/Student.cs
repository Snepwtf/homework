﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10_Collection_Generic
{
    public class Student : IComparable<Student>

    {
        public Student(string name,int assessment)
        {
            Assessment = assessment;
            Name = name;
        }
        public string Name { get; set; }
        public string TestName { get; set; }
        public DateTime TestDate { get; set; }
        public int Assessment { get; set; }

        public int CompareTo(Student other)
        {
            Student p = other as Student;
            if (p != null)
                return this.Assessment.CompareTo(p.Assessment);
            else
                throw new Exception("Невозможно сравнить два объекта");
        }
    }
}
