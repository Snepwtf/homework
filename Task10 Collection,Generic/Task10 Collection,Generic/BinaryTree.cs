﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Task10_Collection_Generic
{
    public class BinaryTree<T> : IEnumerator<T>
        where T : IComparable<T>
    {
        public Node<T> Root { get; set; }
        public int Count { get; set; }

        public T Current => throw new NotImplementedException();

        object IEnumerator.Current => throw new NotImplementedException();

        public void Add(T data)
        {
            var node = new Node<T>(data);
            if (Root == null)
            {
                Root = node;
                Count = 1;
                return;
            }
            Root.Add(data);
            Count++;
        }
        public IEnumerable<T> GetElements()
        {
            return TravelNode(Root);
        }
        //GetReversedEnumarator
        public IEnumerable<T> GetReversedElements()
        {
            return ReversedTravelNode(Root);
        }
        public IEnumerable<T> TravelNode(Node<T> node)
        {
            if (node.Left != null)
            {
                foreach (T i in TravelNode(node.Left))
                {
                    yield return i;
                }

            }
            yield return node.Data;

            if (node.Right != null)
            {
                foreach (T i in TravelNode(node.Right))
                {
                    yield return i;
                }
            }
        }
        public IEnumerable<T> ReversedTravelNode(Node<T> node)
        {
            if (node.Right != null)
            {
                foreach (T i in ReversedTravelNode(node.Right))
                {
                    yield return i;
                }
            }
            yield return node.Data;

            if (node.Left != null)
            {
                foreach (T i in ReversedTravelNode(node.Left))
                {
                    yield return i;
                }
            }

        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            throw new NotImplementedException();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public BinaryTree(IComparer comparer)//компаратор
        {

        }
        public BinaryTree(IEnumerable<T> enumerable)//элемент компэрбл
        {
            foreach(var tr in enumerable)
            {
                Add(tr);
            }
        }
        public BinaryTree(IEnumerable<T> enumerable, IComparer comparer)
        {

        }

    }
}
