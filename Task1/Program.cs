﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace Task1
{
    public class Program
    {
        /// <summary>
        /// Точка входа в приложение.
        /// </summary>
        /// <param name="args">массив входных аргументов</param>
        public static void Main(string[] args)
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
            

            var parser = new Parser();


            if (args.Length == 0) // Если args пуст,то вводим данные вручную
            {
                Console.WriteLine("Введите данные вам координаты : ");
                while (true)
                {
                    try
                    {
                        string input = Console.ReadLine(); //Ввод из консоли
                        Console.WriteLine(parser.Parsing(input));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Введите оси координат X и Y через запятую");
                    }


                }
            }
            else
            {
                string path;

                path = args[0];//если аргумент по индексу 0 содержит путь к текстовому файлу то читаем из него данные

                using (StreamReader sr = new StreamReader(path))
                {
                    string fileString;
                    try
                    {
                        while ((fileString = sr.ReadLine()) != null)// считываем все строки из файла
                        {
                            Console.WriteLine(parser.Parsing(fileString));
                        }

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Текстовый файл должен содержать координаты по оси X и Y в формате 11.111,11.111");
                    }

                }
            }
           

            Console.ReadLine();
        }

       
        

    }

}




