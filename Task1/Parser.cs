﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    public class Parser
    { 
        /// <summary>
      /// Редактирует входные данные для вывода в требуемом формате.
      /// </summary>
      /// <param name="input">число из консоли</param>
        public string Parsing(string input)
        {
            CultureInfo culture = new CultureInfo("ru-RU");

            string[] strings = input.Split(',');

            decimal firstnumber = decimal.Parse(strings[0]);
            decimal secondnumber = decimal.Parse(strings[1]);

            string coordinates = String.Format(culture, "X:{0} Y:{1}", firstnumber, secondnumber);
            return coordinates;

        }
    }
}
