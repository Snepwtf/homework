﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_2_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите целое число для получения его значения в двоичной системе счисления");
            try
            {
                int inputNumber = int.Parse(Console.ReadLine());
                Console.WriteLine(Convert.ToString(inputNumber, 2));
            }
            catch (Exception)
            {
                Console.WriteLine("Пожалуйста,вводите исключительно целые числа!");
                Console.ReadLine();
                return;
            }
            

            Console.WriteLine("Введите целое число для получения его значения с помощью супер алгоритма разработанного совместно с иранскими учёными");
            try
            {
                int inputNumberForConvertMethod = int.Parse(Console.ReadLine());
                Console.WriteLine(ConvertMethod(inputNumberForConvertMethod));
            }
            catch (Exception)
            {
                Console.WriteLine("Для работы нашего супер алгоритма требуются исключительно целочисленные значения!");
                Console.ReadLine();
                return;
            }




            Console.ReadLine();
        }

        public static int ConvertMethod(int x)
        {
            return x < 2 ? x % 2 : (x % 2) + 10 * ConvertMethod(x / 2);
        }
    }
}
