﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    public class EuclideanClass
    {
        public int Euclidean(int a, int b)
        {
            while (a != b)
            {
                if (a > b)
                {
                    a -= b;

                }
                else
                {
                    b -= a;

                }
            }
            return a;
        }
        public int Euclidean(int a, int b, int c)
        {
            int result = Euclidean(Euclidean(a, b), c);
            return result;
        }
        public int Euclidean(int a, int b, int c, int d)
        {
            int result = Euclidean(Euclidean(Euclidean(a, b), c), d);
            return result;
        }

        public int BinaryEuclidean(int a, int b)
        {
            if (a == 0)
            {
                return b;
            }
            else if (b == 0)
            {
                return a;
            }
            else if (a == b)
            {
                return a;
            }
            else if (a % 2 == 0 && b % 2 == 0)
            {
                return 2 * BinaryEuclidean(a / 2, b / 2);
            }
            else if (a % 2 == 0 && b % 2 == 1)
            {
                return BinaryEuclidean(a / 2, b);
            }
            else if (a % 2 == 1 && b % 2 == 0)
            {
                return BinaryEuclidean(a, b / 2);
            }
            else if (a % 2 == 1 && b % 2 == 1 && a > b)
            {
                return BinaryEuclidean((a - b) / 2, b);
            }
            else if (a % 2 == 1 && b % 2 == 1 && a < b)
            {
                return BinaryEuclidean((b - a) / 2, a);
            }
            else
            {
                return b;
            }
        }
        public int TimerForBinaryNod(int a, int b, out double msec)
        {
            var timer = new Stopwatch();
            timer.Start();
            int result = BinaryEuclidean(a, b);
            timer.Stop();
            msec = timer.ElapsedMilliseconds;

            return result;
        }

        public int TimerForEuclidean(int a, int b, out double msec)
        {
            var timer = new Stopwatch();
            timer.Start();
            int result = Euclidean(a, b);
            timer.Stop();
            msec = timer.ElapsedMilliseconds;

            return result;

        }

    }
}
