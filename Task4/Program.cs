﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 2 числа для вычисления НОД");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            var example = new EuclideanClass();

            Console.WriteLine($"Наибольший общий делитель чисел равен {example.Euclidean(a, b)}");

            example.TimerForEuclidean(624960, 49104, out double time);
            Console.WriteLine("Время затраченное на выполнение метода равно : " + time);

            Console.ReadLine();


        }

    }
}
