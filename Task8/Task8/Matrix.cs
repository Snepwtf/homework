﻿using System;
using System.Collections.Generic;

namespace Task8
{
    public class Matrix : IEquatable<Matrix>
    {

        private int[,] M { get; set; }

        public Matrix(int[,] m)
        {
            M = m;
        }
        public Matrix(int a, int b) 
        {
            M = new int[a, b];
        }
        public static Matrix GetEmpty(int a,int b)
        {
            return new Matrix(a, b);
        }

        public int this[int i, int j]
        {
            get
            {
                return M[i, j];
            }
            set
            {
                M[i, j] = value;
            }
        }


        public static Matrix Sum(Matrix a, Matrix b)
        {
            if (a.M.GetLength(0) == b.M.GetLength(0) && a.M.GetLength(1) == b.M.GetLength(1))
            {
                var result = a + b;
                return result;
            }
            else
            {
                throw new MatrixException("Матрица А не равна матрице В", a.M.GetLength(0), b.M.GetLength(0), a.M.GetLength(1), b.M.GetLength(1));
            }

        }
        public static Matrix Subtraction(Matrix a, Matrix b)
        {
            if (a.M.GetLength(0) == b.M.GetLength(0) && a.M.GetLength(1) == b.M.GetLength(1))
            {
                var result = a - b;
                return result;
            }
            else
            {
                throw new MatrixException("Матрица А не равна матрице В", a.M.GetLength(0), b.M.GetLength(0), a.M.GetLength(1), b.M.GetLength(1));

            }

        }
        public static Matrix Multiply(Matrix a, Matrix b)
        {
            if (a.M.GetLength(1)== b.M.GetLength(0))
            {
                var result = a * b;
                return result;
            }
            else
            {
                throw new MatrixException("Матрица А не совместима с матрицей B", a.M.GetLength(0), b.M.GetLength(0), a.M.GetLength(1), b.M.GetLength(1));

            }

        }

       

        public static Matrix operator *(Matrix a, Matrix b)
        {
            Matrix newMatrix = new Matrix(a.M.GetLength(0),b.M.GetLength(1));
            for (int i = 0; i < a.M.GetLength(0); i++)
                for (int j = 0; j < b.M.GetLength(1); j++)
                    for (int k = 0; k < b.M.GetLength(0); k++)
                        newMatrix[i, j] += a[i, k] * b[k, j];

            return newMatrix;
        }
        public static Matrix operator +(Matrix a, Matrix b)
        {
            Matrix newMatrix = new Matrix(a.M);
            for (int i = 0; i < a.M.GetLength(0); i++)
            {
                for (int j = 0; j < b.M.GetLength(1); j++)
                {
                    newMatrix[i, j] = a[i, j] + b[i, j];
                }
            }
            return newMatrix;
        }
        public static Matrix operator -(Matrix a, Matrix b)
        {
            Matrix newMatrix = new Matrix(a.M);
            for (int i = 0; i < a.M.GetLength(0); i++)
            {
                for (int j = 0; j < b.M.GetLength(1); j++)
                {
                    newMatrix[i, j] = a[i, j] - b[i, j];
                }
            }
            return newMatrix;
        }
        public override string ToString()
        {
            string result = "";
            for (int i = 0; i < M.GetLength(0); i++)
            {
                for (int j = 0; j < M.GetLength(1); j++)
                {
                    result += M[i, j] + "\t";

                }
                result += "\n";
            }
            return result;

        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Matrix);
        }

        public bool Equals(Matrix other)
        {
            return other != null &&
                   EqualityComparer<int[,]>.Default.Equals(M, other.M);
        }

        public override int GetHashCode()
        {
            return -2048098088 + EqualityComparer<int[,]>.Default.GetHashCode(M);
        }
    }
}
