﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix matrix1 = new Matrix(new int[3, 4]
            {
               {7,2,3,3},
               {5,1,5,5},
               {1,2,3,9}
            });
            Matrix matrix2 = new Matrix(new int[4, 2]
            {
               {6,1},
               {4,1},
               {1,2},
               {1,2}
            });
            Matrix matrix3 = new Matrix(new int[4, 2]
           {
               {7,1},
               {10,1},
               {11,22},
               {13,23}
           });
            try
            {
                Console.WriteLine(Matrix.Multiply(matrix1, matrix2));
                Console.WriteLine(Matrix.Sum(matrix3, matrix2));
                //Console.WriteLine(Matrix.Sum(matrix1, matrix2));

            }
            catch (MatrixException ex)
            {
                Console.WriteLine(ex);
            }



            Console.ReadLine();

        }
    }
}
