﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task8
{
    class MatrixException : Exception
    {
        public int AMatrixLenght { get; set; }
        public int BMatrixLenght { get; set; }
        public int AMatrixWidth { get; set; }
        public int BMatrixWidth { get; set; }
        public MatrixException(int aMatrixLenght, int bMatrixLenght, int aMatrixWidth, int bMatrixWidth) : base()
        {
            AMatrixLenght = aMatrixLenght;
            BMatrixLenght = bMatrixLenght;
            AMatrixWidth = aMatrixWidth;
            BMatrixWidth = bMatrixWidth;
        }
        public MatrixException(string message, int aMatrixLenght, int bMatrixLenght, int aMatrixWidth, int bMatrixWidth) : base(message)
        {
            AMatrixLenght = aMatrixLenght;
            BMatrixLenght = bMatrixLenght;
            AMatrixWidth = aMatrixWidth;
            BMatrixWidth = bMatrixWidth;

        }
    }
}
