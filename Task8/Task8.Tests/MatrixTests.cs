﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task8.Tests
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void Matrix_Sum_Test()
        {
            //arrange
            Matrix matrix1 = new Matrix(new int[4, 2]
            {
               {6,1},
               {4,1},
               {1,2},
               {1,2}
            });
            Matrix matrix2 = new Matrix(new int[4, 2]
            {
               {7,1},
               {10,1},
               {11,22},
               {13,23}
            });
            Matrix expected = new Matrix(new int[4, 2]
            {
               {13,2},
               {14,2},
               {12,24},
               {14,25}
            });

            //act
            Matrix actual = Matrix.Sum(matrix1, matrix2);

            //assert
            Assert.IsTrue(actual.Equals(expected));

        }
    }
}
