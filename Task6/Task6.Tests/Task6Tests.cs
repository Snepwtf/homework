﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Task6.Tests
{
    [TestClass]
    public class Task6Tests
    {
        [TestMethod]
        public void TestMethod1()
        {
            //arrange

            Vector vector1 = new Vector(10, 6, 80);
            Vector vector2 = new Vector(15, 19, 10);
            Vector expectedVector = new Vector(25, 25, 90);

            //act
            Vector actualVector = vector1 + vector2;

            //assert
            Assert.AreEqual(expectedVector, actualVector);
        }
        [TestMethod]
        public void TestMethod2()
        {
            //arrange
            Vector vector1 = new Vector(10, 6, 80);
            Vector vector2 = new Vector(15, 19, 10);
            Vector expectedVector = new Vector(-5, -13, 70);

            //act
            Vector actualVector = vector1 - vector2;

            //assert
            Assert.AreEqual(expectedVector, actualVector);

        }
        [TestMethod]
        public void TestMethod3()
        {
            //arrange
            Vector vector1 = new Vector(10, 6, 80);
            Vector vector2 = new Vector(15, 19, 10);
            double expected = 1064;

            //act
            double actual = (vector1 * vector2);

            //assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod4()
        {
            //arrange
            Vector vector1 = new Vector(10, 6, 80);
            double number = 9;
            Vector expectedVector = new Vector(90,54,720);

            //act
            Vector actualVector = (vector1 * number);
            //assert
            Assert.AreEqual(expectedVector,actualVector);
        }
    }
}
