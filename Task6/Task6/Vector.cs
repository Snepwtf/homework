﻿namespace Task6
{
    public class Vector
    {
        private double x, y, z;

        public Vector(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        /// <summary>
        /// Сложение 2х векторов
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static Vector operator +(Vector V1, Vector V2)
        {
            Vector vector3 = new Vector(V1.x + V2.x, V1.y + V2.y, V1.z + V2.z);
            return vector3;
        }
        /// <summary>
        /// Вычитание 2х векторов
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static Vector operator -(Vector V1, Vector V2)
        {
            Vector vector3 = new Vector(V1.x - V2.x, V1.y - V2.y, V1.z - V2.z);
            return vector3;
        }
        /// <summary>
        /// Скалярное произведение 2х векторов
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static double operator *(Vector V1, Vector V2)
        {
            return (V1.x * V2.x + V1.y * V2.y + V1.z * V2.z);
        }
        /// <summary>
        /// Вектор умноженный на число
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static Vector operator *(Vector V1, double number)
        {
            Vector vector3 = new Vector(number * V1.x, number * V1.y, number * V1.z);
            return vector3;
        }

        public override string ToString()
        {
            return $"{x},{y},{z}";
        }

        public override bool Equals(object obj)
        {
            return obj is Vector vector &&
                   x == vector.x &&
                   y == vector.y &&
                   z == vector.z;
        }

        public override int GetHashCode()
        {
            int hashCode = 373119288;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            hashCode = hashCode * -1521134295 + z.GetHashCode();
            return hashCode;
        }
    }
}
