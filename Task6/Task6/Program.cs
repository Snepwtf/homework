﻿using System;


namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите данные для построения 1-го трёхмерного вектора");
            Console.WriteLine("X: ");
            double x1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Y: ");
            double y1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Z: ");
            double z1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите число для того чтобы умножить на него данный вектор");
            double number = double.Parse(Console.ReadLine());
            Vector vector1 = new Vector(x1, y1, z1);
            Console.WriteLine($"1-ый вектор равен - {vector1}");

            Console.WriteLine("Введите данные для построения 2-го трёхмерного вектора");
            Console.WriteLine("X: ");
            double x2 = double.Parse(Console.ReadLine());
            Console.WriteLine("Y: ");
            double y2 = double.Parse(Console.ReadLine());
            Console.WriteLine("Z: ");
            double z2 = double.Parse(Console.ReadLine());
            Vector vector2 = new Vector(x2, y2, z2);
            Console.WriteLine($"2-ой вектор равен - {vector2}");

            Vector vector3 = (vector1 + vector2);
            Console.WriteLine($"Сумма данных векторов равна - {vector3}");

            Vector vector4 = (vector1 - vector2);
            Console.WriteLine($"Разность данных векторов равна - {vector4}");

            double result = (vector1 * vector2);
            Console.WriteLine($"Скалярное произведение 2-ух векторов равно {result}");

            Vector vector5 = (vector1 * number);
            Console.WriteLine($"{vector1} вектор умноженный на число {number} равен {vector5}");



            Console.ReadLine();

        }
    }
}
