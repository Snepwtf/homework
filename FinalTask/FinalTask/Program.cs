﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace FinalTask
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Human> humanList = new List<Human>()
            {
                new Human{Age=24,Gender="Female",Name="Loeya",Country="Sweden"},
                new Human{Age=30,Gender="Male",Name="Vova",Country="Russia"},
                new Human{Age=41,Gender="Female",Name="Evelyn",Country="USA"},
                new Human{Age=25,Gender="Male",Name="Kriss",Country="Australia"}
            };
            List<Car> carList = new List<Car>()
            {
                new Car{Name="Mercedes",Age=2009,Country="German",Color="Black"},
                new Car{Name="Lada",Age=2019,Country="Russia",Color="White"},
                new Car{Name="Audi",Age=2010,Country="German",Color="Grey"},
                new Car{Name="Aston Martin",Age=2015,Country="England",Color="Blue"}
            };

            #region Сериализация раз
            var jsonFormatter = new DataContractJsonSerializer(typeof(List<Human>));

            using (var file = new FileStream("human.json", FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(file, humanList);
            }

            using (var file = new FileStream("human.json", FileMode.OpenOrCreate))
            {
                var listToConsole = jsonFormatter.ReadObject(file) as List<Human>;

                if (listToConsole != null)
                {
                    foreach (var human in humanList)
                    {
                        Console.WriteLine(human);
                    }
                }
            }
            #endregion

            Console.WriteLine();

            #region Сериализация два
            var jsonFormatter1 = new DataContractJsonSerializer(typeof(List<Car>));

            using (var file = new FileStream("car.json", FileMode.OpenOrCreate))
            {
                jsonFormatter1.WriteObject(file, carList);
            }

            using (var file = new FileStream("car.json", FileMode.OpenOrCreate))
            {
                var listToConsole = jsonFormatter1.ReadObject(file) as List<Car>;

                if (listToConsole != null)
                {
                    foreach (var car in carList)
                    {
                        Console.WriteLine(car);
                    }
                }
            }
            #endregion


        }
    }
}
