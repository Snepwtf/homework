﻿using System.Runtime.Serialization;

namespace FinalTask
{
    [DataContract]
    public class Human
    {
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Имя - {Name}\tВозраст - {Age}\tСтрана - {Country}\tПол - {Gender}";
        }
    }
}
