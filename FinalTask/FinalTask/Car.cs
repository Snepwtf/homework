﻿using System.Runtime.Serialization;

namespace FinalTask
{
    [DataContract]
    public class Car
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Color { get; set; }
        [DataMember]
        public string Country { get; set; }

        public override string ToString()
        {
            return $"Название - {Name}\tГод выпуска - {Age}\tСтрана производитель - {Country}\tЦвет - {Color}";
        }
    }
}
