﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ControlTask
{
    [Serializable]
    public static class TaskClass
    {
        
        public static List<string> GetStrings(this List<string> stringList)
        {
           
            var result = stringList.Where(l => l.Contains("a")).ToList();
            foreach (string s in result)
            {
                System.Console.WriteLine(s);
            }

            return result;

        }

    }
}
