﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
    public class ProgramConverter : IConvertible
    {
        public string ConvertToCSharp(string convertString)
        {
            string result = convertString + "csharp";
            return result;
        }

        public string ConvertToVB(string convertString)
        {
            string result = convertString + "vb";
            return result;
        }
    }
}
