﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            ProgramHelper helper = new ProgramHelper();
            var result = helper.CheckCodeSyntax((helper.ConvertToVB("Language is ")), "vb");
            Console.WriteLine(result);

            ProgramConverter[] array = new ProgramConverter[4] { new ProgramConverter(), new ProgramHelper(), new ProgramConverter(), new ProgramHelper() };
            for (int i = 0; i < array.Length; i++)
            {
                ICodeChecker codeChecker = null;
                codeChecker = array[i] as ICodeChecker;
                if (codeChecker != null)
                {
                    codeChecker.CheckCodeSyntax("this is vb code", "vb");
                    array[i].ConvertToVB("this language is ");
                }
                else
                {
                    array[i].ConvertToCSharp("this language is");
                    array[i].ConvertToVB("this language is");
                }
            }
        }
    }
}
