﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
    interface ICodeChecker
    {
        bool CheckCodeSyntax(string checkString, string language);
    }
}
