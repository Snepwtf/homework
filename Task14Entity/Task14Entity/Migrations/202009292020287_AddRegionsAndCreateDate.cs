﻿namespace Task14Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRegionsAndCreateDate : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Region", newName: "Regions");
            AddColumn("dbo.Customers", "CreateDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "CreateDate");
            RenameTable(name: "dbo.Regions", newName: "Region");
        }
    }
}
