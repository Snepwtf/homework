﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task14Entity
{
    public class CreditCards
    {
        public int Id{ get; set; }
        public int CardNumber { get; set; }

        public DateTime DateOff { get; set; }
        public string CardHolder { get; set; }
        public Employees Employees { get; set; }
    }
}
