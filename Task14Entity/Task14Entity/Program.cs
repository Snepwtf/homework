﻿using System;
using System.Linq;

namespace Task14Entity
{
    class Program
    {
        static void Main(string[] args)
        {
            using (DbCon db = new DbCon())
            {
                var result = db.Products.Where(prod => prod.CategoryID == 4).ToList();


                foreach (Products product in result)
                    Console.WriteLine("{0} - {1}", product.ProductName, product.Categories.CategoryName);
            }

            using(DbCon db = new DbCon())
            {
                var result = db.Order_Details.Join(db.Orders,
                    od => od.OrderID,
                    ord => ord.OrderID,
                    (od, ord) => new
                    {
                        OrderId = od.OrderID,
                        CustName = ord.Customers.CompanyName,
                        ProductName = od.Products.ProductName
                    });

                foreach (var inf in result)
                {
                    Console.WriteLine("{0},custName - {1},prodName - {2}",inf.OrderId,inf.CustName,inf.ProductName);
                }
            }
        }
    }
}
