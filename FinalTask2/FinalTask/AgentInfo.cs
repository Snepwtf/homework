﻿using System;
using System.Xml.Serialization;

namespace FinalTask
{
    [Serializable]
    [XmlType("AGENT_INFO")]
    public class AgentInfo
    {
        
        [XmlElement("ISN")]
        public long Isn { get; set; }

        [XmlElement]
        public string ShortName { get; set; }

        [XmlElement]
        public string FullName { get; set; }

        [XmlElement]
        public bool IsBank { get; set; }

        [XmlElement("ClassISN")]
        public long ClassIsn { get; set; }



    }
}
