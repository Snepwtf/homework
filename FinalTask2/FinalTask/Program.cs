﻿using System.IO;
using System.Xml.Serialization;

namespace FinalTask
{
    class Program
    {
        static void Main(string[] args)
        {
            PortalUserInfo userInfoCollection = null;
            string path = "PORTAL_USER_INFO.xml";

            XmlSerializer deserialize = new XmlSerializer(typeof(PortalUserInfo));

            StreamReader reader = new StreamReader(path);

            userInfoCollection = (PortalUserInfo)deserialize.Deserialize(reader);
            
            reader.Close();
        }
    }
}
