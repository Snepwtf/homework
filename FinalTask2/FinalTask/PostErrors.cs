﻿using System;
using System.Xml.Serialization;

namespace FinalTask
{
    [Serializable]
    [XmlType("POST_ERRORS")]
    public class PostErrors
    {
        [XmlElement]
        public long All { get; set; }

        [XmlElement]
        public long Critical { get; set; }
    }
}
