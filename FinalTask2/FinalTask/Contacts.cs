﻿using System;
using System.Xml.Serialization;

namespace FinalTask
{
    [Serializable]
    [XmlType("CONTACTS")]
    public class Contacts
    {
        [XmlElement]
        public long SmsPhoneNumber { get; set; }

        [XmlElement]
        public bool PhoneNeedActualize { get; set; }
    }
}
