﻿using System;
using System.Xml.Serialization;

namespace FinalTask
{
    [Serializable]
    [XmlRoot("PORTAL_USER_INFO")]
    public class PortalUserInfo
    {
        
        [XmlElement]
        public bool IsEmployee { get; set; }

        [XmlElement]
        public string Company { get; set; }

        [XmlElement("FilialISN")]
        public long FilialIsn { get; set; }

        [XmlElement("POST_ERRORS")]
        public PostErrors PostErrors { get; set; }

        [XmlElement("CONTACTS")]
        public Contacts Contacts { get; set; }

        [XmlElement("AGENT_INFO")]
        public AgentInfo AgentInfo { get; set; }
    }
}
