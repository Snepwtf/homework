﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;

namespace Task1.Tests.Task1.Core
{

    [TestClass]
    public class TaskTests
    {

        [TestMethod]
        public void TestMethod1()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
            //Arrange
            string expected = "X:123,999 Y:123,56723";

            //Act

            var testParser = new Parser();
            string actual = testParser.Parsing("123.999,123.56723");

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod2()
        {
            //Arrange
            Exception exception = null;
            var testParser = new Parser();

            //Act
            try
            {
                testParser.Parsing("asdsafasd");
            }
            catch (Exception exeption1)
            {
                exception = exeption1;
            }


            //Assert
            Assert.IsNotNull(exception);
        }
    }
}
